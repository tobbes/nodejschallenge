const path = '/Users/tobias/saved.json';
const http = require("http");
const url = require('url')
const querystring = require('querystring');
var queryRegexp = "KEY=(\\w+)$";
var savedData = {};
process.on('exit',function() {
  console.log("About to exit!")
  save();
});


const filereader = require('fs');

//Load previously saved data on startup
// If it exists.
// If this throws an exception assume no data has been saved before
//This may not be the proper way to to this in nodejs
try {
const stat = filereader.statSync(path);
if(stat !== undefined && stat.isFile()) {
  savedData = require(path);
}
}

  catch(e) {
    console.log(e);
    console.log("No previous data");
  }

http.createServer(handleReq).listen(8081);

console.log("Server running on port 8081");


// I do not know if this is the proper way to do it, but process does
// not seem to exit properly when process is killed, so I intercept the signal
// and exit the process manually
process.on('SIGTERM',function() {
console.log("GOT SIGTERM");
//save();
process.exit();

});

process.on('SIGINT',function() {
  console.log("Got SIGINT");
  //save();
  process.exit();
});









function handlePost(request,response) {
  var body = [];

  request.on("data", function(chunk) {
    console.log("Requestdata started");
      body.push(chunk);
  });

  request.on('end', function() {
      console.log("Request ended");
      body = Buffer.concat(body).toString();
      var hash = JSON.parse(body);

      //You may not try to add a key which already exists
      for(var key in hash) {
        if(savedData[key] !== undefined) {
          response.writeHead(406,{'Content-Type' : "text/plain"})
          response.write("KEY " + key + "already exists try PUT or DELETE the key before adding it");
          response.end();
          return;
        }
      }

      const d = new Date();
      for (var key in hash) {
        savedData[key] = {'VALUE' : hash[key],'DATE' : d.toLocaleString()}
        savedData[key]['DATE'] = d.toLocaleString();
      }
      console.log(savedData);

      //STATUSCODE: RESOURCE CREATED
      response.writeHead(201,{'Content-Type': 'text/plain'});
      //response.write(body);
      response.write("Successfully added values");
      response.end();

  });




}


 function handleReq(request,response) {
     //console.log(request.method)
	 var method = request.method;

    if(method === 'PUT') {
      handlePut(request,response);
      return;
    }
     else if(method === 'GET'){
	      console.log("GET");
        handleGet(request,response);
        return;
     }

     else if(method === 'POST') {
       console.log("POSTREQUEST");
       handlePost(request,response);
       return;
     }

     else  if(method === 'PUT'){
	      console.log('PUT');
        return;
     }

     else if(method === 'DELETE'){
	      console.log("DELETE")
        handleDelete(request,response);
        return;
     }

     else {
	      console.log("UNKNOWN REQUESTTYPE");
        return;
     }



     return;
  }


  function handlePut(request,response) {
    var data = [];
      request.on('data',function(datachunk) {
        data.push(datachunk);
      } );

      request.on('end', function() {
      data =   Buffer.concat(data).toString();
        var toUpdate = JSON.parse(data)
        for(var key in toUpdate) {
          if(savedData[key] === undefined) {
            response.writeHead(406, {'Content-Type' : 'text/plain'});
            response.write("Key: " + key + " does not exist, try creating");
            response.end();
            return;
          }
          for(var key in toUpdate) {
            savedData[key] = toUpdate[key];
          }
          response.writeHead(200,{'Content-Type' : "text/plain"});
          response.write("Keys successfully updated");
          response.end();
          return;


        }

      });


  }

  function handleDelete(request,response) {
    const query = parseQuery(request);
    const toDelete = query['KEY'];
    delete savedData['KEY'];
    response.writeHead(200,{'Content-Type' : 'text/plain'});
    response.write(KEY + " successfully removed");
    response.end();

  }

  function parseQuery(request) {
    const parsedURL = url.parse(request.url);
    const queryPart = querystring.parse(parsedURL.query);
    return queryPart;

  }


  function handleGet(request,response) {
    const query = parseQuery(request);
    const key = query["KEY"];

    //If no key is provied return all key-value pairs
    if(key === undefined ) {
    response.writeHead(200,{'Content-Type' : 'application/json'});
    response.write(JSON.stringify(savedData));
    response.end();
    return;
  }


    const valueToReturn = savedData[key];

    if(valueToReturn !== undefined) {
    response.writeHead(200,{'Content-Type' : 'application/json'})
    response.write(valueToReturn + "");
    response.end();
    return;
  }
/*  else if(key === undefined) {
    response.writeHead(400,{'Content-Type' : 'text/plain'});
    response.write("PARAMETER 'KEY' must be set");
    response.end();
  } */

  else {
      response.writeHead(200,{'Content-Type' : 'text/plain'});
      response.write("KEY NOT FOUND");
      response.end();
  }

  }



  function save() {
    const toSave = JSON.stringify(savedData);
    var fs = require('fs');
    fs.writeFileSync(path,toSave);
    }
